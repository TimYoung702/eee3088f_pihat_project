EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Light Sensor Subsystem"
Date "2021-05-31"
Rev "1.2"
Comp "EEE3088F Group 56"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Potentiometer_Digital:AD5272BCP U2
U 1 1 60B853D6
P 4300 3900
F 0 "U2" H 4300 4581 50  0000 C CNN
F 1 "AD5272BCP" H 4300 4490 50  0000 C CNN
F 2 "Package_CSP:LFCSP-WD-10-1EP_3x3mm_P0.5mm_EP1.64x2.38mm" H 5600 3450 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD5272_5274.pdf" H 4300 3900 50  0001 C CNN
	1    4300 3900
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Optical:LDR03 R8
U 1 1 60B87DFE
P 5100 4350
F 0 "R8" H 5170 4396 50  0000 L CNN
F 1 "LDR03" H 5170 4305 50  0000 L CNN
F 2 "OptoDevice:R_LDR_10x8.5mm_P7.6mm_Vertical" V 5275 4350 50  0001 C CNN
F 3 "http://www.elektronica-componenten.nl/WebRoot/StoreNL/Shops/61422969/54F1/BA0C/C664/31B9/2173/C0A8/2AB9/2AEF/LDR03IMP.pdf" H 5100 4300 50  0001 C CNN
	1    5100 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4200 5100 3800
Wire Wire Line
	5100 3800 4800 3800
$Comp
L Device:R R9
U 1 1 60B8912D
P 5500 3800
F 0 "R9" V 5293 3800 50  0000 C CNN
F 1 "2k" V 5384 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5430 3800 50  0001 C CNN
F 3 "~" H 5500 3800 50  0001 C CNN
	1    5500 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 60B89F35
P 5800 4200
F 0 "R11" H 5870 4246 50  0000 L CNN
F 1 "7.5k" H 5870 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 4200 50  0001 C CNN
F 3 "~" H 5800 4200 50  0001 C CNN
	1    5800 4200
	1    0    0    -1  
$EndComp
$Comp
L eee3088f-group56-rescue:LT1368CN8#PBF-LT1368CN8#PBF IC1
U 1 1 60B8B3CF
P 6350 3600
AR Path="/60B8B3CF" Ref="IC1"  Part="1" 
AR Path="/60B7EAE0/60B8B3CF" Ref="IC1"  Part="1" 
F 0 "IC1" H 7050 3865 50  0000 C CNN
F 1 "LT1368CN8#PBF" H 7050 3774 50  0000 C CNN
F 2 "lib:DIP794W53P254L1016H394Q8N" H 7600 3700 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/LT1368CN8#PBF.pdf" H 7600 3600 50  0001 L CNN
F 4 "LINEAR TECHNOLOGY - LT1368CN8#PBF - Operational Amplifier, Single, 1 Amplifier, 400 kHz, 0.065 V/s, +/- 5V to +/- 15V, DIP, 8 Pins" H 7600 3500 50  0001 L CNN "Description"
F 5 "3.937" H 7600 3400 50  0001 L CNN "Height"
F 6 "Linear Technology" H 7600 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "LT1368CN8#PBF" H 7600 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "584-LT1368CN8#PBF" H 7600 3100 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Analog-Devices-Linear-Technology/LT1368CN8PBF?qs=ytflclh7QUUcU28iUdaGyg%3D%3D" H 7600 3000 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7600 2900 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7600 2800 50  0001 L CNN "Arrow Price/Stock"
	1    6350 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3800 5350 3800
Connection ~ 5100 3800
Wire Wire Line
	5650 3800 5800 3800
Wire Wire Line
	5800 4050 5800 3800
Connection ~ 5800 3800
Wire Wire Line
	5800 3800 6350 3800
Wire Wire Line
	4800 3700 5100 3700
$Comp
L Device:R R12
U 1 1 60B8F837
P 5950 3100
F 0 "R12" V 5743 3100 50  0000 C CNN
F 1 "7.5k" V 5834 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5880 3100 50  0001 C CNN
F 3 "~" H 5950 3100 50  0001 C CNN
	1    5950 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60B8EC2E
P 5650 2800
F 0 "R10" H 5720 2846 50  0000 L CNN
F 1 "2k" H 5720 2755 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5580 2800 50  0001 C CNN
F 3 "~" H 5650 2800 50  0001 C CNN
	1    5650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3700 3550 3700
Wire Wire Line
	3800 3800 3550 3800
Wire Wire Line
	5800 3100 5650 3100
Connection ~ 5650 3100
Wire Wire Line
	5650 3100 5650 2950
Wire Wire Line
	5650 3100 5650 3700
Wire Wire Line
	5650 3700 6350 3700
Wire Wire Line
	6100 3600 6350 3600
Wire Wire Line
	6100 3100 6100 3600
$Comp
L Device:R R14
U 1 1 60B96D28
P 6750 2750
F 0 "R14" H 6820 2796 50  0000 L CNN
F 1 "100k" H 6820 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6680 2750 50  0001 C CNN
F 3 "~" H 6750 2750 50  0001 C CNN
	1    6750 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2900 6750 3100
Wire Wire Line
	6750 3100 7050 3100
Wire Wire Line
	6750 2600 6750 2450
Wire Wire Line
	6350 3900 6200 3900
Wire Wire Line
	4200 4900 4300 4900
Connection ~ 5100 4900
Wire Wire Line
	5100 4900 5800 4900
Connection ~ 5800 4900
Wire Wire Line
	5800 4900 6200 4900
Wire Wire Line
	4200 4900 3950 4900
Connection ~ 4200 4900
Text GLabel 3950 4900 0    50   Input ~ 0
GND
Text GLabel 6750 2450 0    50   Input ~ 0
GND
Wire Wire Line
	5100 2500 5100 3700
Wire Wire Line
	4300 2500 4300 3400
Wire Wire Line
	5650 2650 5650 2500
Wire Wire Line
	7750 2500 7750 3600
Text GLabel 4300 2500 0    50   Input ~ 0
5Vcc
Text GLabel 5100 2500 0    50   Input ~ 0
5Vcc
Text GLabel 7750 2500 0    50   Input ~ 0
5Vcc
Text GLabel 5650 2500 0    50   Input ~ 0
3.3Vcc
Text HLabel 3550 3700 0    50   Input ~ 0
R1_SCL
Text HLabel 3550 3800 0    50   Input ~ 0
R1_SDA
Text HLabel 7050 3100 2    50   Input ~ 0
GPIO_19
$Comp
L Device:R R13
U 1 1 60BAC49A
P 6450 3100
F 0 "R13" V 6243 3100 50  0000 C CNN
F 1 "100" V 6334 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6380 3100 50  0001 C CNN
F 3 "~" H 6450 3100 50  0001 C CNN
	1    6450 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 3100 6600 3100
Connection ~ 6750 3100
Wire Wire Line
	6300 3100 6100 3100
Connection ~ 6100 3100
Connection ~ 4300 4900
Wire Wire Line
	4300 4900 5100 4900
Wire Wire Line
	3800 4100 2950 4100
Wire Wire Line
	2950 4100 2950 3400
Wire Wire Line
	2950 3400 4300 3400
Connection ~ 4300 3400
$Comp
L pspice:C C9
U 1 1 60B60CDE
P 4600 4700
F 0 "C9" V 4285 4700 50  0000 C CNN
F 1 "1u" V 4376 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4600 4700 50  0001 C CNN
F 3 "~" H 4600 4700 50  0001 C CNN
	1    4600 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 4400 4200 4900
Wire Wire Line
	4300 4400 4300 4700
Wire Wire Line
	5100 4500 5100 4900
Wire Wire Line
	5800 4350 5800 4900
Wire Wire Line
	6200 3900 6200 4900
Wire Wire Line
	4350 4700 4300 4700
Connection ~ 4300 4700
Wire Wire Line
	4300 4700 4300 4900
Wire Wire Line
	4850 4700 4850 4100
Wire Wire Line
	4850 4100 4800 4100
$EndSCHEMATC
