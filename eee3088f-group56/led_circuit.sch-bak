EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "LED Circuit"
Date "2021-06-01"
Rev "1.0"
Comp "EEE3088F Group 56"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R17
U 1 1 60B670D3
P 4850 1500
F 0 "R17" H 4920 1546 50  0000 L CNN
F 1 "330" H 4920 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4780 1500 50  0001 C CNN
F 3 "~" H 4850 1500 50  0001 C CNN
	1    4850 1500
	1    0    0    -1  
$EndComp
$Comp
L eee3088f-group56-rescue:2N2222A-2N2222A Q1
U 1 1 60B68EDF
P 4450 2450
AR Path="/60B68EDF" Ref="Q1"  Part="1" 
AR Path="/60B7DE9C/60B68EDF" Ref="Q1"  Part="1" 
F 0 "Q1" H 4988 2496 50  0000 L CNN
F 1 "2N2222" H 4988 2405 50  0000 L CNN
F 2 "lib:TO127P254X732-3" H 5000 2300 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/2n2222a/microsemi" H 5000 2200 50  0001 L CNN
F 4 "Trans GP BJT NPN 50V 0.8A 3-Pin TO-18" H 5000 2100 50  0001 L CNN "Description"
F 5 "" H 5000 2000 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 5000 1900 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222A" H 5000 1800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222A" H 5000 1700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222A?qs=TXMzd3F6EylR6f6YErRW3Q%3D%3D" H 5000 1600 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222A" H 5000 1500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222a/microsemi" H 5000 1400 50  0001 L CNN "Arrow Price/Stock"
	1    4450 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 60B6B08E
P 4850 1900
F 0 "D3" V 4889 1782 50  0000 R CNN
F 1 "LED" V 4798 1782 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4850 1900 50  0001 C CNN
F 3 "~" H 4850 1900 50  0001 C CNN
	1    4850 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R18
U 1 1 60B6BDDB
P 4850 3000
F 0 "R18" H 4920 3046 50  0000 L CNN
F 1 "330" H 4920 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4780 3000 50  0001 C CNN
F 3 "~" H 4850 3000 50  0001 C CNN
	1    4850 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 60B6C1EA
P 4100 2450
F 0 "R15" V 3893 2450 50  0000 C CNN
F 1 "330" V 3984 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4030 2450 50  0001 C CNN
F 3 "~" H 4100 2450 50  0001 C CNN
	1    4100 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 2850 4850 2750
Wire Wire Line
	4850 2150 4850 2050
Wire Wire Line
	4850 1750 4850 1650
Wire Wire Line
	4850 1350 4850 1200
Wire Wire Line
	4450 2450 4250 2450
Wire Wire Line
	3950 2450 3700 2450
Wire Wire Line
	4850 3150 4850 3300
Text GLabel 4850 1200 0    50   Input ~ 0
5Vcc
Text GLabel 4850 3300 0    50   Input ~ 0
GND
$Comp
L Device:R R19
U 1 1 60B7164B
P 4850 4800
F 0 "R19" H 4920 4846 50  0000 L CNN
F 1 "330" H 4920 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4780 4800 50  0001 C CNN
F 3 "~" H 4850 4800 50  0001 C CNN
	1    4850 4800
	1    0    0    -1  
$EndComp
$Comp
L eee3088f-group56-rescue:2N2222A-2N2222A Q2
U 1 1 60B71659
P 4450 5750
AR Path="/60B71659" Ref="Q2"  Part="1" 
AR Path="/60B7DE9C/60B71659" Ref="Q2"  Part="1" 
F 0 "Q2" H 4988 5796 50  0000 L CNN
F 1 "2N2222" H 4988 5705 50  0000 L CNN
F 2 "lib:TO127P254X732-3" H 5000 5600 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/2n2222a/microsemi" H 5000 5500 50  0001 L CNN
F 4 "Trans GP BJT NPN 50V 0.8A 3-Pin TO-18" H 5000 5400 50  0001 L CNN "Description"
F 5 "" H 5000 5300 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 5000 5200 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222A" H 5000 5100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222A" H 5000 5000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222A?qs=TXMzd3F6EylR6f6YErRW3Q%3D%3D" H 5000 4900 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222A" H 5000 4800 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222a/microsemi" H 5000 4700 50  0001 L CNN "Arrow Price/Stock"
	1    4450 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 60B7165F
P 4850 5200
F 0 "D4" V 4889 5082 50  0000 R CNN
F 1 "LED" V 4798 5082 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 4850 5200 50  0001 C CNN
F 3 "~" H 4850 5200 50  0001 C CNN
	1    4850 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R20
U 1 1 60B71665
P 4850 6300
F 0 "R20" H 4920 6346 50  0000 L CNN
F 1 "330" H 4920 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4780 6300 50  0001 C CNN
F 3 "~" H 4850 6300 50  0001 C CNN
	1    4850 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 60B7166B
P 4100 5750
F 0 "R16" V 3893 5750 50  0000 C CNN
F 1 "330" V 3984 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4030 5750 50  0001 C CNN
F 3 "~" H 4100 5750 50  0001 C CNN
	1    4100 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 6150 4850 6050
Wire Wire Line
	4850 5450 4850 5350
Wire Wire Line
	4850 5050 4850 4950
Wire Wire Line
	4850 4650 4850 4500
Wire Wire Line
	4450 5750 4250 5750
Wire Wire Line
	3950 5750 3700 5750
Wire Wire Line
	4850 6450 4850 6600
Text GLabel 4850 4500 0    50   Input ~ 0
5Vcc
Text GLabel 4850 6600 0    50   Input ~ 0
GND
$Comp
L Device:R R22
U 1 1 60B7783A
P 7800 3050
F 0 "R22" H 7870 3096 50  0000 L CNN
F 1 "330" H 7870 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 3050 50  0001 C CNN
F 3 "~" H 7800 3050 50  0001 C CNN
	1    7800 3050
	1    0    0    -1  
$EndComp
$Comp
L eee3088f-group56-rescue:2N2222A-2N2222A Q3
U 1 1 60B77848
P 7400 4000
AR Path="/60B77848" Ref="Q3"  Part="1" 
AR Path="/60B7DE9C/60B77848" Ref="Q3"  Part="1" 
F 0 "Q3" H 7938 4046 50  0000 L CNN
F 1 "2N2222" H 7938 3955 50  0000 L CNN
F 2 "lib:TO127P254X732-3" H 7950 3850 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/2n2222a/microsemi" H 7950 3750 50  0001 L CNN
F 4 "Trans GP BJT NPN 50V 0.8A 3-Pin TO-18" H 7950 3650 50  0001 L CNN "Description"
F 5 "" H 7950 3550 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 7950 3450 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222A" H 7950 3350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222A" H 7950 3250 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222A?qs=TXMzd3F6EylR6f6YErRW3Q%3D%3D" H 7950 3150 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222A" H 7950 3050 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222a/microsemi" H 7950 2950 50  0001 L CNN "Arrow Price/Stock"
	1    7400 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 60B7784E
P 7800 3450
F 0 "D5" V 7839 3332 50  0000 R CNN
F 1 "LED" V 7748 3332 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 7800 3450 50  0001 C CNN
F 3 "~" H 7800 3450 50  0001 C CNN
	1    7800 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R23
U 1 1 60B77854
P 7800 4550
F 0 "R23" H 7870 4596 50  0000 L CNN
F 1 "330" H 7870 4505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 4550 50  0001 C CNN
F 3 "~" H 7800 4550 50  0001 C CNN
	1    7800 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 60B7785A
P 7050 4000
F 0 "R21" V 6843 4000 50  0000 C CNN
F 1 "330" V 6934 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6980 4000 50  0001 C CNN
F 3 "~" H 7050 4000 50  0001 C CNN
	1    7050 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	7800 4400 7800 4300
Wire Wire Line
	7800 3700 7800 3600
Wire Wire Line
	7800 3300 7800 3200
Wire Wire Line
	7800 2900 7800 2750
Wire Wire Line
	7400 4000 7200 4000
Wire Wire Line
	6900 4000 6650 4000
Wire Wire Line
	7800 4700 7800 4850
Text GLabel 7800 2750 0    50   Input ~ 0
5Vcc
Text GLabel 7800 4850 0    50   Input ~ 0
GND
Text HLabel 3700 2450 0    50   Input ~ 0
GPIO_17
Text HLabel 3700 5750 0    50   Input ~ 0
GPIO_22
Text HLabel 6650 4000 0    50   Input ~ 0
GPIO_27
$EndSCHEMATC
