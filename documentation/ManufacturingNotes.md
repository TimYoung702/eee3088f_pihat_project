Please follow the following instructions to manufacture the PCB:

A. Create the Gerber Files.
1. Clone the [eee3088f-group56](eee3088f-group56/) repo.
2. Open KiCAD. (free, download [here](https://www.kicad.org/download/))
3. In KiCAD, open the [eee3088f-group56](eee3088f-group56/eee3088f-group56.pro) project file.
4. Click on "Gerber Files"
5. Save Gerber Files.

B. Order the PCB.
1. Take the Gerber Files and the BOM (found [here](https://gitlab.com/TimYoung702/eee3088f_pihat_project/-/blob/master/documentation/BillofMaterials.csv)) to your prefered PCB Manufacturer.
